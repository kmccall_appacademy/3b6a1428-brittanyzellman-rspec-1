def add(num_1, num_2)
  num_1 + num_2
end

def subtract(num_1, num_2)
  num_1 - num_2
end

def sum(array_nums)
  if array_nums.length == 0
    return 0
  else
    return array_nums.reduce(:+)
  end
end

def multiply(num_1, num_2)
  num_1 * num_2
end

def multiplies(array_nums)
  if array_nums.length == 0
    return 0
  end
  array_nums.reduce(:*)
end

def factorial(num)
  if num >= 2
    array_nums = (1..num).to_a
    return multiplies(array_nums.reverse!)
  else
    return num
  end
end

def power(num_1, num_2)
  num_1 ** num_2
end 
