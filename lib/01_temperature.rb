# require 'byebug'

def ftoc(degree)
  # byebug
  (degree.to_f - 32) * (5.0 / 9.0)
end

def ctof(degree)
  (degree.to_f * 1.8) + 32
end
