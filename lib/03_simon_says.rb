require "byebug"

def echo (string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, num = 2)
  arr_of_string = []
  while num > 0
    arr_of_string << string
    num -= 1
  end
  arr_of_string.join(" ")
end

def start_of_word(string, num = 1)
    string[0...num]
end

def first_word(string)
  arr_words = string.split(" ")
  arr_words[0]
end

def titleize(string)
  arr_words = string.split(" ")
  if arr_words.length == 1
    return string.capitalize
  end
  arr_words.map.with_index do |word, i|
    if word == "over"
      next 
    end
    if (word.length > 3 || i == 0)
      word.capitalize!
    end
  end
  arr_words.join(" ")
end
