require "byebug"

def translate(string)
  translated = []
  array_of_words = string.split(" ")
  array_of_words.each do |word|
    word.chars.each.with_index do |ch, idx|
      if is_vowel?(ch.downcase)
        translated << pigify(word, idx)
        break
      end
    end
  end
  translated.join(" ")
end

def is_vowel?(char)
  vowels = ['a', 'e', 'i', 'o', 'u']
  if vowels.include?(char)
    return true
  end
  false
end

def pigify(word, index)
  # debugger
  if word[index] == 'u'
    index += 1
  end 
  if index == 0
    p  "#{word}ay"
    return "#{word}ay"
  else
    word_arr = word.chars
    word_arr.rotate!(index)
    return "#{word_arr.join}ay"
  end
end
